# Guía de contribución y control de código en Gitlab

Guía para contribuir en los repositorios de gitlab del **Deparatamento de Educación a Distancia**. Esta guía se aplica a cualquier otro repositorio externo o diferente a este grupo.


## Indice
- [Instalación rápida](#instalación-rápida)
- [Creando un repositorio](#creando-un-repositorio)

## **Instalación rápida** #
Guía de instalación y configuración de Git en nuestro equipo.

1. Terminar de leer esta guía.
2. Tener una cuenta en gitlab.
3. Instalar algun cliente git segun sea tu sistema operativo:
    - [Git bash en windows](https://git-scm.com/downloads)
    - [Git CLI en MAC](http://blog.teamtreehouse.com/introduction-to-the-mac-os-x-command-line)
    - [Linux distros](http://www.howtogeek.com/140679/beginner-geek-how-to-start-using-the-linux-terminal).
4. Configurar nuestra cuenta en gitlab (Estos pasos los podemos completar siguiendo la [guía de aquí](https://docs.gitlab.com/ce/gitlab-basics/start-using-git.html) en ingles)

## **Creando un repositorio** #
> **Importante** haber realizado los pasos anteriores en la guía de instalación.

1. Crear un proyecto en la cuenta del Gitlab de [teamdistancia](https://gitlab.com/teamdistancia) (O pedir a un administrador que nos cree un repositorio)
![Crear repositorio](./images/create-project.PNG)

2. En este caso se nos abrirá una nueva pagina que nos ayudará a crear el repositorio, este nos pide el nombre del nuevo proyecto (tambien conocido como repositorio de código):
![Crear repositorio](./images/create-project-view.PNG)


## **Contribuir en un proyecto existente** #

Contribur en un proyecto implica la colaboración de uno o mas miembros del equipo en el desarrollo de un proyecto, esta contribución puede ser en muchos aspectos, tales como documentación, código fuente, detección de errores, comentar, etc. Por otro lado,estas reglas se aplican a cualquier proyecto alojado en Gitlab siempre y cuando su licencia lo permita _(p.ej: MIT, GPL y otros)_. 

Para este ejercicio se tomó como ejemplo el proyecto de [**moodle**](https://gitlab.com/teamdistancia/moodle) qu hace parte de 
Departamento de educación a distancia.

### **Pasos**

> **Nota:** Estos pasos se aplican a cualquier repositorio de gitlab.

1. Entramos al proyecto al que deseamos contribuir.
![Crear repositorio](./images/repo-example.PNG)

2. Haremos un fork de este repositorio (Hacemos click en el boton que dice Fork o Bifurcar) y clickeamos en _fork to_ (nombre de mi cuenta)
Bifucar:
![Bifurcar código](./images/fork.PNG)
Asignar bifurcación _(fork)_ a nuestra cuenta:
![Bifurcar a cuenta](./images/fork-it.PNG)

> **Nota:** El proceso de bifurcación creará un proyecto espejo de _Moodle_ en nuestra cuenta, este tendrá una replica exacta del código en el momento en que se hizo la bifurcación.

2. Clonamos este repositorio en nuestra cuenta local. Para esto debemos abrir la terminal de comandos _(Git Bash en windows, o la CMD)_ y escribimos el siguiente comando: 

```bash
git clone https://gitlab.com/<username>/moodle.git
# Donde <username> es nuestra cuenta de usuario de gitlab
# por ejemplo: https://gitlab.com/anlijudavid/moodle.git
```

Como ya has visto en el paso anterior, la copia espejo o bifurcada está en nuestra cuenta de Gitlab permitiendonos modificar y cargar el codigo que deseamos afectar. El proceso de clonación migra este repositorio a nuestra computadora _(o al equipo en el que ejecutamos el clone)_ haciendo una descarga completa del repositorio completo, es decir, Moodle en nuestro equipo.

3. Agregamos / Editamos los archivos necesarios.

4. [Agregamos nuestros cambios al repositorio: ](http://www.kernel.org/pub/software/scm/git/docs/git-add.html)

```bash
git add .
```
> **Nota**: Podemos añadir tanto archivos individuales como en conjunto. El punto (`.`) al final del comando añade todos los archivos dentro de la carpeta.

5. Hacemos `commit` de nuestros cambios con algun mensaje descriptivo pero corto en la primera linea:

```bash
git commit -m 'Agregando archivos base para solucionar problema de compatiblidad del core'
```

6. Repetimos los pasos **4** a **5** cada vez que agreguemos más cambios

```bash
# Más cambios
git commit -m 'Solucionado error de instanciación de clase principal'

# .... mas cambios en el código
git commit -m 'Nueva caracteristica para analisis de datos de usuarios'
```

7. Una vez que estemos contentos con nuestros cambios, realizamos un push al repositorio:

```bash
git push origin master
```
Con este comando estamos sincronizando nuestro branch local (master) a un branch con el mismo nombre en gitlab. 

Nuestro push está en nuestro repo junto con todos los commits que fueron realizados.

8. Hacemos un pull request. Para esto, en la imagen a continuación se muestra el menú derecho, vemos un boton que dice `Merge Request`:

![Merge request](./images/button-merge.PNG)

- Le hacemos clic y se nos mostrará una pantalla. Click en el botón verde que dice `New merge Request`. En ella escribiremos qué feature(s) se agregan con nuestros cambios y por qué debería ser aceptado.

- Se abre automaticamente una ventana para realizazar comparaciones en ambos proyectos (El bifurcado de tu cuenta y el original). Dejamos la configuración por defecto, es decir, seleccionamos como `master` desde origen y `master` en el repo oficial:
![Merge request](./images/compare.PNG)

- Presionamos el botón verde `Compare branches and continue`.
- Escribimos el titulo y descripción que defina todo lo que hemos realizado anteriormente. En el titulo es importante destacar en pocas palabras lo que se ha hecho, mientras que en la descripción colocamos una descripción completa de todo. 
> **Sugerencia:** Recuerda que la descripción es dinamica y es compatible con el lenguaje Markdown, podrás colocar titulos, tablas, parrafos, listas...

- Luego de terminar de escribir nuestros cambios _(paso anterior)__ se procede a enviar todo al repositorio oficial, presionar el botón verde Submit merge request:
![Submit merge request](./images/submit-changes.PNG)

- **Finalización**: La organización o dueño del proyecto verá esta solicitud, y podrá comentar. Por otro lado, incluso antes de unir los cambios, podemos modificar nuestro merge request. Una vez hecho el merge, gitlab nos envía una notificación, y podremos ver nuestros cambios en el repositorio principal.

9. Luego de que el merge request sea aceptado por el lider del proyecto, debemos actualizar nuestro repo con el base. Para eso, primero agregaremos el nuevo repo remoto _(Este paso solo se realiza una vez por equipo)_.

```bash
git remote add upstream https://gitlab.com/teamdistancia/moodle.git
```
> **Importante:** La dirección especificada en el comando anterior es la del proyecto oficial, NO es la ruta que está en tu cuenta personal.

10. Actualizamos los cambios en nuestro repo local y nuestro repo de gitlab

```bash
git fetch upstream
git checkout master
git merge upstream/master
git push origin master
```

Si queremos contribuir nuevamente al mismo u otro repo, solo tendremos que seguir los pasos del 3 al 10 (si ya hemos clonado el repositorio) o del 1 al 10 (si recién vamos a clonarlo).

> **Nota:** Recuerda que estos pasos son aplicables a cualquier proyecto alojado en gitlab.